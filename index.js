var React = require('react');
var ReactDOM = require('react-dom');

/*

 state = {
     num_of_posts: 2,
     wallposts: {
         wallpost: {
             id: 1,
             post: "......",
             comments: ["comm1", "comm2", "comm3"]
         },

         wallpost2: {
             id: 2,
             post: "....",
             comments: ["comm1"]
         }

     }
 }

*/


var CommentBox = React.createClass({


    onPostClick: function() {

        console.log("Why did you click")
    },

    render: function() {
        return (
        <div>
        <textarea>{this.props.comment}</textarea>
                <button onClick={this.onPostClick}>Post</button>
        </div>
        )
    }
})

var CommentList = React.createClass({
      render: function() {
          return (
            <div>
              <CommentBox comment="I love you"></CommentBox>
              <CommentBox comment="I have you"></CommentBox>
            </div>
            )
        }
    })

ReactDOM.render(
    <CommentList />,
    document.getElementById('container')
);
